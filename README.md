#### Sommaire

- [Installation](#installation)
    - [Interface](#1-interface)
    - [Données](#2-données)
    - [Particularité macOS](#particularité-macos)
    - [Particularité Windows](#particularité-windows)
- [Présentation du projet](#description-du-projet)

# Installation

## 1. Interface

Télécharger l'archive correspondante à votre système d'exploitation et l'**extraire** dans `~/Documents`

- [Version Windows](dist%2Fwindows%2Fannotation_MSSEG3.zip)
- [Version MacOs](dist%2FmacOS%2Fannotation_MSSEG3.zip)
- [Version Linux](dist%2Flinux%2Fannotation_MSSEG3.zip)

Vous devez maintenant avoir un dossier `annotation_MSSEG3` dans `~/Documents`.

## 2. Données

- Récupérez les données fournies via le lien envoyé par mail.
- Vous allez télécharger une archive nommée `data_MSSEG3.zip`. Il faut **extraire** cette archive dans le même dossier
  que le reste des fichiers fournis, c'est-à-dire
  dans `~/Documents/annotation_MSSEG3`.
- Votre dossier `annotation_MSSEG3` devrait maintenant ressembler à cela :<br><br>
  ![4_folder.png](img%2F4_folder.png)<br><br>
- Pour terminer, lancer l'application à partir de l'executable `Interface_SegHelper` présent dans ce
  dossier `~/Documents/annotation_MSSEG3`.
- Une archive contenant un ensemble de fausses données (images vides) est disponible pour tester le fonctionnement de
  l'interface. [Télécharger ici](dist%2Fdata_MSSEG3.zip).

## Particularité macOS

### Pré-requis

- itk-SNAP : Installer à partir de l'installateur officiel.
    - Pour les Mac avec processeurs intel (
      x86) : [Télécharger ici.](dist%2FmacOs%2Fitksnap-4.2.0-20240422-Darwin-x86_64.dmg)
    - Pour les Mac avec processeurs M1, … (
      arm64) : [Télécharger ici.](dist%2FmacOs%2Fitksnap-4.2.0-20240422-Darwin-arm64.dmg)

### Remarques

- À présent, vous pouvez lancer l'application à partir de l'executable `Interface_SegHelper` présent dans le
  dossier `~/Documents/annotation_MSSEG3`.
- Attention, l'application est incompatible avec Safari, certains éléments ne s'afficheront pas.  
  Ouvrir Firefox ou Chrome et copier l'adresse de l'application dans la barre : `127.0.0.1:8000`

## Particularité Windows

### Remarques

- Votre antivirus pourrait bloquer/supprimer l'executable au moment de l'extraction de
  l'archive `annotation_MSSEG3.zip`.  
  Dans ce cas, ajoutez une exception ou désactivez votre antivirus le temps de l'extraction.

# Description du projet

- L'application proposée est une interface graphique qui permet d'ouvrir facilement toutes les acquisitions et les
  segmentations d'un patient
  dans le logiciel itksnap.

- L'interface propose une liste des patients qu'il reste à annoter une fois le bouton "Scan data folder" appuyé.<br><br>
  ![1_scan_folder.png](img%2F1_scan_folder.png)<br><br>

- Une fois un patient sélectionné dans la table, il est possible d'ouvrir les acquisitions dans itksnap avec toutes les
  autres
  séquences à dispositions.
    - Si aucune segmentation n'existe, un masque vide est créé et ouvert sur le T2.<br><br>
      ![2_open_in_itksnap.png](img%2F2_open_in_itksnap.png)<br><br>
    - Si une segmentation précédemment créée existe, elle est ré-ouverte sur le T2.<br><br>
      ![3_already_segmented.png](img%2F3_already_segmented.png)<br><br>
- Il est également possible d'ouvrir le dossier contenant l'acquisition et les masques dans un explorateur de fichier
  avec le bouton associé. Puis d'ouvrir manuellement les séquences souhaitées avec le visualisateur de son choix.
- Une fois l'ensemble des annotations effectuées, utiliser le bouton "Export all annotations" pour créer une archive
  contenant tous les masques créés. Cette archive est générée dans le dossier de l'application,
  c'est-à-dire `~/Documents/annotation_MSSEG3`.
